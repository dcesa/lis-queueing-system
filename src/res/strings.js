const strings = {
	generation: {
		client: {
			title: 'Get Your Number Here',
			button1: 'LABORATORY REQUEST',
			button2: 'LABORATORY RESULT',
			text1: 'For patients that wants to request laboratory exam',
			text2: 'For patients that will get their laboratory results'
		},
		personnel: {
			title: 'QUEUEING',
			button1: 'NEXT',
			button2: 'CASHIER',
			button3: 'PHLEBO',
			label1: 'Receptionist',
			label2: 'Cashier',
			label3: 'Phlebo Personnel',
			label4: 'Print Queue Number',
		}
	},
	monitor: {
		title: 'NOW SERVING',
		code: 'P04',
		module: 'PHLEBO',
	}
};

export default strings;
