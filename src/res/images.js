const images = {
	logos: {
		doctor: require('images/doctor.png'),
		testResult: require('images/medical-history.png'),
	}
};

export default images;