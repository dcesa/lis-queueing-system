import colors from './colors';

const palettes = {
	attention: {
		color: 'white',
		fontSize: 36
	},
	heading1: {
		color: 'white',
		fontSize: 24,
	},
	heading2: {
		color: 'white',
		fontSize: 22,
	},
	heading3: {
		color: 'white',
		fontSize: 20,
	},
	label: {
		fontSize: 18,
		fontWeight: 500
	},
	text: {
		fontSize: 16
	},
	layoutHeader: {
		backgroundColor: colors.white
	},
	button: {
		backgroundColor: colors.darkWhite,
		borderRadius: 5,
		padding: '10px 20px'
	}
};

export default palettes;