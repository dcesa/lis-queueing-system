const colors = {
	theme: '#6666CC',
	violet: '#6666CC',
	gray: '#666666',
	silver: '#999999',
	darkWhite: '#EEEEEE',
};

export default colors;