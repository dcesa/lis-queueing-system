import React from 'react';
import { Layout } from 'antd';
import { BrowserRouter as Router, Route, Switch, withRouter  } from 'react-router-dom';
import Content from './content';
import Footer from './footer';


class MainLayout extends React.Component {
	render() {
		return (
			<Router>
				<Layout style={{ backgroundColor: 'white' }}>
					<Content />
					<Footer />
				</Layout>
			</Router>
		);
	}
}

export default MainLayout;