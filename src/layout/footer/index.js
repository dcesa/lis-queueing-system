import React from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;

class LayoutFooter extends React.Component {
	render(){
		return (
			<Footer style={{ backgroundColor: 'white' }}>
				{/* This is footer */}
			</Footer>
		);
	}
}

export default LayoutFooter;