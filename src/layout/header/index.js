import React from 'react';
import { Layout } from 'antd';
import palettes from 'res/palettes';

const { Header } = Layout;

/** @type {{ header: React.CSSProperties }} */
const styles = {
	header: {
		textAlign: 'center',
		height: 100,
		lineHeight: '100px',
		...palettes.layoutHeader
	}
};

class LayoutHeader extends React.Component {
	render(){
	

		return (
			<Header style={styles.header}>
				<div>
					<h1 style={palettes.attention}>General Hospital</h1>
				</div>
			</Header>
		);
	}
}

export default LayoutHeader;