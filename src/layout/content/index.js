import React from 'react';
import { Layout } from 'antd';
import { Route, Switch, withRouter, BrowserRouter } from 'react-router-dom';
import ClientNumberGeneration from 'main/generation/client';
import PersonnelNumberGeneration from 'main/generation/personnel';
import Monitor from 'main/monitor';

const { Content } = Layout;

class LayoutContent extends React.Component {
	render() {
		return (
			<BrowserRouter>
				<Content>
					<Switch>
						<Route exact path="/client" component={ClientNumberGeneration} />
						<Route exact path="/personnel" component={PersonnelNumberGeneration} />
						<Route exact path="/monitor" component={Monitor} />
						{/* <Route component={ClientNumberGeneration} /> */}
					</Switch>
				</Content>
			</BrowserRouter>
		);
	}
}

export default withRouter(LayoutContent);