import React from 'react';
import MyLayout from './layout';

function App() {
  return (
		<MyLayout />
  );
}

export default App;
