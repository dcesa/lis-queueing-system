import React from 'react';
import { Typography, Icon } from 'antd';
import { ReactComponent as ArrowIcon } from 'icons/right-arrow.svg';
import palettes from 'res/palettes';
import colors from 'res/colors';

const { Text } = Typography;

/** @type {{ 
 * button: React.CSSProperties, 
 * icon: React.CSSProperties,
 * text: React.CSSProperties
 * }} */
const style = {
	button: {
		...palettes.button,
		fontSize: 20,
		backgroundColor: colors.theme,
		position: 'relative',
		textAlign: 'center',
		borderRadius: 10
	},
	icon: { 
		position: 'absolute',
		left: 20,
		top: 12,
		fontSize: 28
	},
	text: {
		fontSize: 20,
		color: 'white'
	}
};

class NextButton extends React.Component {
	render() {
		const { label } = this.props;

		return (
			<div style={style.button}>
				<Icon component={ArrowIcon} style={style.icon}/>
				<Text style={style.text}>{label}</Text>
			</div>
		);
	}
}

export default NextButton;