import React from 'react';
import { Typography, Icon } from 'antd';
import { ReactComponent as PrinterIcon } from 'icons/printer.svg';
import palettes from 'res/palettes';

const { Text } = Typography;

/** @type {{ 
 * button: React.CSSProperties, 
 * icon: React.CSSProperties,
 * text: React.CSSPropertie
 *  }} */
const style = {
	button: {
		...palettes.button,
		position: 'relative',
		textAlign: 'center',
		borderRadius: 10
	},
	icon: { 
		position: 'absolute',
		left: 20,
		top: 15,
		fontSize: 24
	},
	text: {
		fontSize: 20
	}
};

class PrintButton extends React.Component {
	render() {
		const { label } = this.props;

		return (
			<div style={style.button}>
				<Icon component={PrinterIcon} style={style.icon}/>
				<Text style={style.text}>{label}</Text>
			</div>
		);
	}
}

export default PrintButton;