import React from 'react';
import { Row, Col, Divider, Typography } from 'antd';
import Header from 'layout/header';
import strings from 'res/strings';
import palettes from 'res/palettes';

import PrintButton from './button/print';
import NextButton from './button/next';
import colors from 'res/colors';

const { Text } = Typography;

/** @type {{ wrapper: React.CSSProperties, labelContainer: React.CSSProperties }} */
const style = {
	wrapper: {
		width: '80%',
		margin: 'auto'
	},
	labelContainer: {
		borderBottomColor: colors.gray,
		borderBottomStyle: 'solid',
		borderBottomWidth: 1,
		marginBottom: 30,
		paddingBottom: 10
	}
};

class PersonnelNumberGeneration extends React.Component {
  render() {
		return (
			<div>
				<Header heading={strings.generation.personnel.title} />
				<div style={style.wrapper}>
					<br />
					<br />
					<Row>
						<div style={style.labelContainer}>
							<Text style={palettes.label}>
								{strings.generation.personnel.label1}
							</Text>
						</div>
					</Row>
					<Row gutter={24}>
						<Col span={5}>
							<NextButton label={strings.generation.personnel.button1} />
						</Col>
						<Col span={5}>
							<PrintButton label={strings.generation.personnel.button2} />
						</Col>
						<Col span={5}>
							<PrintButton label={strings.generation.personnel.button3} />
						</Col>
					</Row>
					<br />
					<br />
					<Row>
						<div style={style.labelContainer}>
							<Text style={palettes.label}>
								{strings.generation.personnel.label2}
							</Text>
						</div>
					</Row>
					<Row gutter={24}>
						<Col span={5}>
							<NextButton label={strings.generation.personnel.button1} />
						</Col>
						<Col span={5}>
							<PrintButton label={strings.generation.personnel.button3} />
						</Col>
					</Row>
					<br />
					<br />
					<Row>
						<div style={style.labelContainer}>
							<Text style={palettes.label}>
								{strings.generation.personnel.label3}
							</Text>
						</div>
					</Row>
					<Row gutter={24}>
						<Col span={5}>
							<NextButton label={strings.generation.personnel.button1} />
						</Col>
					</Row>
				</div>
			</div>
		);
	}
}

export default PersonnelNumberGeneration;