import React from 'react';
import { Row, Col } from 'antd';
import Header from 'layout/header';
import images from 'res/images';
import strings from 'res/strings';


import PrintButton from './button';

class ClientNumberGeneration extends React.Component {
  render() {
		return (
			<div>
				<Header heading={strings.generation.client.title}></Header>
				<Row style={{ marginTop: 150 }}>
					<Col offset={4} span={7}>
						<PrintButton 
							label={strings.generation.client.button1}
							image={images.logos.doctor}
							style={{ float: 'right' }}
							description={strings.generation.client.text1}
						/>
					</Col>
					<Col span={2}>
						<div style={{ textAlign: 'center', marginTop: 90 }}>
							<h3>OR</h3>
						</div>
					</Col>
					<Col span={7}>
						<PrintButton 
							label={strings.generation.client.button2}
							image={images.logos.testResult}
							description={strings.generation.client.text2}
						/>
					</Col>
				</Row>
			</div>
		);
	}
}

export default ClientNumberGeneration;
