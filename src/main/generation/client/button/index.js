import React from 'react';
import { Typography } from 'antd';

const { Text } = Typography;

/** @type {{ 
 * container: React.CSSProperties, 
 * printBtn: React.CSSProperties, 
 * leftPart: React.CSSProperties, 
 * rightPart: React.CSSProperties 
 * description: React.CSSProperties 
 * }} */
const style = {
	container: {
		maxWidth: '400px'
	},
	printBtn: {
		backgroundColor: '#EEEEEE',
		padding: '35px 15px',
		borderRadius: 10,
		fontSize: 24,
		fontWeight: 'bold',
	},
	
	leftPart: {
		display : 'inline-block',
		verticalAlign: 'middle',
		width: '40%'
	},
	
	rightPart: {
		display: 'inline-block',
		verticalAlign: 'middle',
		width: '60%',
	},

	description: {
		display: 'inline-block',
		marginTop: 15,
		fontSize: 18
	}
};



class PrintButton extends React.Component {
	render() {
		const { label, image, description } = this.props;

		return (
			
			<div style={{ ...style.container, ...this.props.style }}>
				<div style={style.printBtn}>
					<div style={style.leftPart}>
						<img src={image} />
					</div>
					<div style={style.rightPart}>
						{label}
					</div>
				</div>
				<div >
					<Text style={style.description}>{description}</Text>
				</div>
			</div>
		);
	}
}

export default PrintButton;