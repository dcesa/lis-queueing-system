import React from 'react';
import { Row, Col } from 'antd';
import Header from 'layout/header';
import { Typography, Divider } from 'antd';
import strings from 'res/strings';
import colors from 'res/colors';

const { Text } = Typography;

/** @type {{ 
 * 		content: React.CSSProperties
 * 		code: React.CSSProperties
 * 	 	serve: React.CSSProperties
 *  }} */
const style = {
	content: {
		width: '100%',
		maxWidth: 500,
		marginTop: 50,
		marginLeft: '70px',
		fontWeight:"bold"
	},
	code: {
		marginLeft:'100px',
		color: colors.black,
		fontSize: 100
	},
	serve:{
	
		fontSize:20
	}
}; 

class QueueMonitor extends React.Component {
  render() {
		return (
			<div>
				<Header heading={strings.monitor.title} />
			<Row>
			<Col span={12}>
			<div style={style.content}>
				<Text style={style.serve}>Now Serving </Text>
					<Text style={style.code}>0147</Text>
					<Divider />
				<Text style={style.serve}>On Counter</Text>
					<Text style={style.code}>0547</Text>
				</div>
			</Col>
			<Col span={10}>
			<div style={style.content}>
			<Text style={style.serve}>Ticket </Text>
				<Text style={style.code}>0137</Text>
				<Divider />
			<Text style={style.serve}>On Process </Text>
				<Text style={style.code}>1514</Text>
			</div>
			</Col>
		  </Row>
		  </div>
			
		);
	}
}

export default QueueMonitor;
